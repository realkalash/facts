package com.example.taski_fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taski_fragments.models.FactModel;
import com.example.taski_fragments.models.RequestModel;
import com.example.taski_fragments.utils.ApiService;
import com.example.taski_fragments.utils.Converter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static android.content.ContentValues.TAG;


public class MainFragment extends Fragment implements AdapterFactsList.OnItemClickListener {
    @BindView(R.id.factsList)
    RecyclerView recyclerView;
    androidx.appcompat.widget.SearchView searchView;
    MenuItem search;
    List<FactModel> factModels = new ArrayList<>();
    AdapterFactsList adapterFactsList;
    private String animalType = "cat";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public MainFragment() {
    }

    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.menu_tolbar);
        Menu menu = toolbar.getMenu();
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (androidx.appcompat.widget.SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                getFromServer(query);
                recyclerView.getAdapter().notifyDataSetChanged();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        getFromServer(animalType);

        return view;
    }

    private void getFromServer(String animalForDownload) {
        Disposable disposable = ApiService.getFactsByAnimalAndAmount(animalForDownload, "100")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<RequestModel>>() {
                    @Override
                    public void accept(List<RequestModel> requestModels) throws Exception {

                        factModels = Converter.convertRequest(requestModels);

                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());

                        recyclerView.setLayoutManager(linearLayoutManager);

                        adapterFactsList = new AdapterFactsList(getContext(), factModels, MainFragment.this);


                        recyclerView.setAdapter(adapterFactsList);

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        throwable.printStackTrace();
                        Toast.makeText(getContext(), "Введите правильно название", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onItemClicked(int position, FactModel factModel, ImageView imageView, TextView textView) {
//        PetDetailFragment petDetailFragment = new PetDetailFragment(factModel, transitionName);
////        Fragment animalDetailFragment = PetDetailFragment.newInstance(animalItem, transitionName);
//        getFragmentManager()
//                .beginTransaction()
////                .addSharedElement(imageView, ViewCompat.getTransitionName(imageView))
//                .addToBackStack(TAG)
//                .add(R.id.content, petDetailFragment)
//                .commit();

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .addSharedElement(imageView, "transitionImage")
                .addSharedElement(textView, "transitionName")
                .addToBackStack(TAG)
                .replace(R.id.rootFrame, new PetDetailFragment(factModel, imageView,textView))
                .commit();
    }
}

package com.example.taski_fragments;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.taski_fragments.models.FactModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterFactsList extends RecyclerView.Adapter<AdapterFactsList.ViewHolder> {

    Context context;
    List<FactModel> factModels;

    OnItemClickListener onItemClickListener;

    public AdapterFactsList(Context context, List<FactModel> factModels, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.factModels = factModels;
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener{
        void onItemClicked(int position, FactModel factModel, ImageView imageView, TextView textView);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_view_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return factModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.nameAnimal)
        TextView name;
        @BindView(R.id.factAnimal)
        TextView factAnimal;
        @BindView(R.id.dateOfCreated)
        TextView dateOfCreated;
        @BindView(R.id.imageView2)
        ImageView imageView;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final int position) {
            name.setText(factModels.get(position).getType());
            factAnimal.setText(factModels.get(position).getText());
            dateOfCreated.setText(factModels.get(position).getCreatedAt());
            if (factModels.get(position).getType().equalsIgnoreCase("cat"))
                Glide.with(context).load(R.drawable.cat).into(imageView);
//                Picasso.get().load(R.drawable.cat).into(imageView);
            if  (factModels.get(position).getType().equalsIgnoreCase("dog"))
                Glide.with(context).load(R.drawable.dog).into(imageView);
//                Picasso.get().load(R.drawable.dog).into(imageView);

            ViewCompat.setTransitionName(imageView, factModels.get(position).getId());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClicked(position,factModels.get(position), imageView, name);
                }
            });

        }
    }
}

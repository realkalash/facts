package com.example.taski_fragments;


import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.taski_fragments.models.FactModel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import io.reactivex.android.schedulers.AndroidSchedulers;

public class PetDetailFragment extends Fragment {

    FactModel factModel;

    TextView desPet;
    TextView namePet;

    TextView textView;

    ImageView imagePet;
    ImageView imageInto;

    PetDetailFragment(FactModel factModel, ImageView imageInto, TextView textView) {
        this.factModel = factModel;
        this.imageInto = imageInto;
        this.textView = textView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        postponeEnterTransition();
        setSharedElementEnterTransition(TransitionInflater.from(getContext()).inflateTransition(android.R.transition.move));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pet_detail, container, false);

        namePet = view.findViewById(R.id.namePet);
        desPet = view.findViewById(R.id.desPet);
        imagePet = view.findViewById(R.id.imagePet);

        namePet.setText(textView.getText());
        desPet.setText(factModel.getText());


        Glide.with(getContext())
                .load(imageInto.getDrawable())
                .into(imagePet);

//        Picasso.get().load(R.drawable.cat)
//                .noFade()
//                .into(imagePet, new Callback() {
//                    @Override
//                    public void onSuccess() {
//                        startPostponedEnterTransition();
//                    }
//
//                    @Override
//                    public void onError(Exception e) {
//                        startPostponedEnterTransition();
//                    }
//                });


        return view;
    }
}

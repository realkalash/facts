package com.example.taski_fragments.models;

public class FactModel {

    private String id;
    private String createdAt;
    private String text;
    private String type;

    public FactModel(String id, String createdAt, String text, String type) {
        this.id = id;
        this.createdAt = createdAt;
        this.text = text;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

package com.example.taski_fragments.utils;

import com.example.taski_fragments.models.FactModel;
import com.example.taski_fragments.models.RequestModel;

import java.util.ArrayList;
import java.util.List;

public class Converter {
    public static List<FactModel> convertRequest(List<RequestModel> requestModel) {

        List<FactModel> facts = new ArrayList<>();

        for (int i = 0; i < requestModel.size(); i++) {

            facts.add(new FactModel(
                    requestModel.get(i).getId(),
                    requestModel.get(i).getCreatedAt(),
                    requestModel.get(i).getText(),
                    requestModel.get(i).getType()));
        }
        return facts;
    }
}

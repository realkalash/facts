package com.example.taski_fragments.utils;

public interface IOnBackPressedFun {
    boolean onBackPressed();
}
